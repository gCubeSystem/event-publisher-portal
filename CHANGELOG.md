This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "event-publisher-portal"

## [v1.1.0-SNAPSHOT]

## [v1.0.2]
- OIDC token only is now used to send events to the orchestrator endpoint (UMA token is no more involved).

## [v1.0.1]
- First release (#19461)
