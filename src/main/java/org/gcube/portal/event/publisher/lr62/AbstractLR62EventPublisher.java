package org.gcube.portal.event.publisher.lr62;

import org.gcube.event.publisher.AbstractEventPublisher;
import org.gcube.event.publisher.Event;
import org.gcube.event.publisher.EventSender;
import org.gcube.event.publisher.HTTPWithOIDCAuthEventSender;
import org.gcube.oidc.rest.OpenIdConnectConfiguration;
import org.gcube.portal.oidc.lr62.LiferayOpenIdConnectConfiguration;
import org.json.simple.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public abstract class AbstractLR62EventPublisher extends AbstractEventPublisher {

    protected static final Log log = LogFactoryUtil.getLog(AbstractLR62EventPublisher.class);

    protected static final boolean PUBLISH_ON_SYSOUT = false;

    public AbstractLR62EventPublisher() {
        super();
    }

    protected EventSender createEventSender() {
        if (PUBLISH_ON_SYSOUT) {
            return new EventSender() {
                @Override
                public void send(Event event) {
                    System.out.println(event);
                }
                @Override
                public String sendAndGetResult(Event event) {
                    System.out.println(event);
                    return null;
                }
                @Override
                public JSONObject retrive(String id) {
                    return null;
                }
            };
        } else {
            OpenIdConnectConfiguration openIdConnectConfiguration = LiferayOpenIdConnectConfiguration
                    .getConfiguration();

            EventPublisherConfiguration eventPublisherConfiguration = EventPublisherConfiguration.getConfiguration();
            return new HTTPWithOIDCAuthEventSender(eventPublisherConfiguration.getEventPublisherURL(),
                    openIdConnectConfiguration.getPortalClientId(), openIdConnectConfiguration.getPortalClientSecret(),
                    openIdConnectConfiguration.getTokenURL());
        }
    }

}