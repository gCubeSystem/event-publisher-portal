package org.gcube.portal.event.publisher.lr62;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import org.gcube.event.publisher.Event;
import org.gcube.vomanagement.usermanagement.GroupManager;
import org.gcube.vomanagement.usermanagement.RoleManager;
import org.gcube.vomanagement.usermanagement.UserManager;
import org.gcube.vomanagement.usermanagement.exception.GroupRetrievalFault;
import org.gcube.vomanagement.usermanagement.exception.RoleRetrievalFault;
import org.gcube.vomanagement.usermanagement.exception.UserManagementSystemException;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayRoleManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayUserManager;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;

public class PortalEvent extends Event {

    private static final long serialVersionUID = -6940345709578482873L;

    protected static final Log log = LogFactoryUtil.getLog(PortalEvent.class);

    protected static final UserManager USER_MANAGER = new LiferayUserManager();
    protected static final GroupManager GROUP_MANAGER = new LiferayGroupManager();
    protected static final RoleManager ROLE_MANAGER = new LiferayRoleManager();

    public static final String TYPE = "portal";
    public static final String USER_ENTRY = "user";
    public static final String GROUP_ENTRY = "group";
    public static final String ROLE_ENTRY = "role";

    public PortalEvent(String name) {
        this(name, null);
    }

    public PortalEvent(String name, Map<String, String> data) {
        super(name, TYPE, EventPublisherConfiguration.getConfiguration().getRootVOName(), data);
    }

    public void setUser(User user) {
        setUser(getUserIdentifier(user));
    }

    protected String getUserIdentifier(User user) {
        return user.getScreenName();
    }

    public void setUser(String user) {
        set(USER_ENTRY, user);
    }

    public String getUser() {
        return (String) get(USER_ENTRY);
    }

    public void setGroup(String group) {
        set(GROUP_ENTRY, group);
    }

    public String getGroup() {
        return (String) get(GROUP_ENTRY);
    }

    public void setGroup(Group group) {
        setGroup(getGroupIdentifier(group));
    }

    protected String getGroupIdentifier(Group group) {
        try {
            return URLEncoder.encode(GROUP_MANAGER.getInfrastructureScope(group.getGroupId()), "UTF-8");
        } catch (UnsupportedEncodingException | UserManagementSystemException | GroupRetrievalFault e) {
            log.warn("Cannot get URL encoded infrastrucure scope for group: " + group, e);
            return null;
        }
    }

    public void setRole(String role) {
        set(ROLE_ENTRY, role);
    }

    public String getRole() {
        return (String) get(ROLE_ENTRY);
    }

    public void setRole(Role role) throws PortalException, SystemException {
        setRole(getRoleIdentifier(role));
    }

    protected String getRoleIdentifier(Role role) throws PortalException, SystemException {
        try {
            return ROLE_MANAGER.getRole(role.getRoleId()).getRoleName();
        } catch (UserManagementSystemException | RoleRetrievalFault e) {
            log.error("Cannot get gCube role for role: " + role, e);
            return null;
        }
    }

}