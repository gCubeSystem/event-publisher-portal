package org.gcube.portal.event.publisher.lr62;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.security.auth.CompanyThreadLocal;
import com.liferay.portal.util.PortalUtil;

public class EventPublisherConfiguration {

    protected static final Log log = LogFactoryUtil.getLog(EventPublisherConfiguration.class);

    public static Map<Long, EventPublisherConfiguration> companyId2Configuration = Collections
            .synchronizedMap(new HashMap<Long, EventPublisherConfiguration>());

    private Long companyId;
    private URL eventPublisherURL;
    private String rootVOName;

    public static synchronized EventPublisherConfiguration getConfiguration(Long companyId) {
        log.trace("Getting config from companyId");
        if (!companyId2Configuration.containsKey(companyId)) {
            companyId2Configuration.put(companyId, new EventPublisherConfiguration(companyId));
        }
        return companyId2Configuration.get(companyId);
    }

    public static synchronized EventPublisherConfiguration getConfiguration(HttpServletRequest request) {
        log.trace("Getting config from request");
        return EventPublisherConfiguration.getConfiguration(PortalUtil.getCompanyId(request));
    }

    public static synchronized EventPublisherConfiguration getConfiguration() {
        log.trace("Getting config from thread local");
        return EventPublisherConfiguration.getConfiguration(CompanyThreadLocal.getCompanyId());
    }

    private EventPublisherConfiguration(Long companyId) {
        log.info("Creating config from companyId: " + companyId);
        this.companyId = companyId;
        try {
            this.eventPublisherURL = new URL(PrefsPropsUtil.getString(companyId, "d4science.event-broker-endpoint"));
        } catch (SystemException | MalformedURLException e) {
            throw new RuntimeException(e);
        }
        log.info("eventPublisherURL=" + getEventPublisherURL());

        try {
            rootVOName = new LiferayGroupManager().getRootVOName();
        } catch (Exception e) {
            log.error("Cannot compute encoded root VO context", e);
        }
    }

    public Long getCompanyId() {
        return companyId;
    }

    public URL getEventPublisherURL() {
        return eventPublisherURL;
    }

    public String getRootVOName() {
        return rootVOName;
    }

}
